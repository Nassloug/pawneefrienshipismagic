#ifndef __TOOLS_H__
#define __TOOLS_H__
int match( const char *str_request,const char *str_regex);
char * get_mime_type(const char * extension);
int get_file_size(int fd);
char *fgets_or_exit(char *buffer,int size,FILE *stream);
void skip_headers(FILE *client);
char * rewrite_url(char * url );
int copy(int in,FILE * out);
# endif
