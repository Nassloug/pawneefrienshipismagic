#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include "socket.h"
#include <sys/wait.h>
#include "tools.h"
#include <stdlib.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "constante.h"

#define PORT 8080

char * serverRoot;


void send_status ( FILE * client , int code , const char * reason_phrase ){
	int t;
	t=fprintf(client,"HTTP/1.1 %d %s \r\n",code,reason_phrase);
	if(t<=0){
	printf("Fermeture du client\n");
	fclose(client);
	exit(0);
	}
	
}

//envoie des reponses ne necitant pas la lecture d'un fichier
void send_response ( FILE * client , int code , const char * reason_phrase ,
const char * message_body ){
	send_status(client ,code ,reason_phrase);
	int t;
	fprintf(client,"Connection : close\r\n");
	fprintf(client,"Content-Type: text/plain \r\n");
	fprintf(client,"Content_Length %d \r\n\r\n",(int)strlen(message_body));
	t=fprintf(client,"%s",message_body);
	if(t<=0){
	printf("Fermeture du client\n");
	fclose(client);
	exit(0);
	}
	
}

//envoie des reponses necesitant la lecture d'un fichier
void send_response_file ( FILE * client , int code , const char * reason_phrase ,
int fd ,char * mime_type){
	send_status(client ,code ,reason_phrase);
	int t=0;
	fprintf(client,"Connection : close\r\n");
	fprintf(client,"Content-Type: %s \r\n",mime_type);
	fprintf(client,"Content_Length %d \r\n\r\n",get_file_size(fd));
	//on envoie le fichier
	t=copy(fd,client);
	if(t<0){
	printf("Fermeture du client\n");
	fclose(client);
	exit(0);
	}
	
}



int check_and_open(const char * url){
	struct stat sb;
	printf("\ncheck:  %s \n",url);
	//permet d'essayé d'ouvrir un fichier
	if(stat(url,&sb)==-1){
		printf("fail stat\n");
		return -1;
	}
	//on teste si le fichier est regulier
	if(S_ISREG(sb.st_mode)==0){
		printf("n'est pas un fichier regulier\n");
		return -1;
	}
	
	return open(url,O_RDONLY);
	
}





int  fileRequest(int fd){
  FILE* client;
  char  url[255];
  char requete[255];
  
  int file;
  int requestOk;
  char buff[TAILLE_BUFF];

  client = fdopen(fd,"w+");
  if(client == NULL) {
    perror("Error opening file");
    return(-1);
  }

  requestOk=0;
  //on essai de lire la ligne de la requete
  printf("%s",fgets_or_exit(buff,TAILLE_BUFF,client));

   memcpy(&requete,buff,strlen(buff));
   //on teste si la requeste est de la bonne syntax
   if(match(buff,"^GET \\S+ HTTP\\/1\\.[10]\\s*$")){
      requestOk=1;
   }

  skip_headers(client);

	if(requestOk){
		printf("Ok\n %s  ",requete);
		//on recupere l'url demendee
		if(sscanf(requete,"%*s %s %*s",url)!=1){perror("sscanf");}
		rewrite_url(url);
		printf("\n%s\n",url);

		//si l'url contient des ".." on indique qui c'est interdit
		if(strstr(url,"..")!=NULL){
			send_response(client,403,"Forbidden","403 Forbidden\n");

		//on check si le fichier existe
		}else if((file=check_and_open(strcat(serverRoot,url)))!=-1){
			send_response_file( client ,200,"OK",file,strrchr(url,'.')+1);
		//sinon on indique que la ressource demande et introuvable
		}else{
			send_response(client,404,"Not Found","404 Not Found\n");
		}
	// si la syntax de la requete n'est pas bonne on envoie l'erreur 400
	}else{
		printf("PASOK\n");
		send_response(client,400,"Bad Request","400 Bad Request\n");
	}

   printf("Avant le flush du message au client \n");  
  fflush(client);
  printf("Envoi d'un message au client \n");  
  close(fd);
  fclose(client);
  return 0;
}




int main(int argc,char * argv[]){
  int socket_serveur;
  int socket_client;
  pid_t pid;
  initialiser_signaux();
  socket_serveur = creer_serveur(PORT);
  
  if(argc!=2){
	return -1;
  }
  serverRoot=argv[1];

  if(socket_serveur==-1){
    return -1;
  }
  while(1){
  	//attente client
    socket_client=accept(socket_serveur,NULL,NULL);
    if(socket_client==-1){
      perror("accept");
    }
    //création d'un fil pour chaque client
    pid = fork();
    if(pid==0){
      return fileRequest(socket_client);
    }
    else if(pid<0){
      perror("pid");
    }
    close(socket_client);
  }
  return 0;
}

