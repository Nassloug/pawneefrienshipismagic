#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <regex.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include "constante.h"


//permet de teste si une chaine coorespond a une regex
int match( const char *str_request,const char *str_regex){

	regex_t regex;
	int reti;
	char msgbuf[100];

	// On compile l'expresion reguliere
	reti = regcomp(&regex, str_regex, REG_EXTENDED);
	if (reti) {
    		fprintf(stderr, "Could not compile regex\n");
		regfree(&regex);
    		return -1;
	}

	// on l'execute
	reti = regexec(&regex, str_request, 0, NULL, 0);
	if (!reti) {
		regfree(&regex);
    		return 1;
	}
	else if (reti == REG_NOMATCH) {
		regfree(&regex);
    		return 0;
	}
	else {
    		regerror(reti, &regex, msgbuf, sizeof(msgbuf));
    		fprintf(stderr, "Regex match failed: %s\n", msgbuf);
		regfree(&regex);
    		return -1;
	}
}

char * get_mime_type(const char * extension){
	char  * mime_type="";
	char  extract;
	FILE* fid;

  // on essai d'ouvrir le fichier /etc/mime.types
  if ((fid = fopen("/etc/mime.types","r+")) == NULL)
  {
     return "text/plain";
  }
  
  //on parcour le fichier a la recherche de la bonne extension
  while (!feof(fid))
  {
     if (fscanf(fid,"%s %s",mime_type, &extract))
     {
     	// si on a trouvee l'extention on renvoi le type mime coorespondant
        if(strcmp(extension,&extract)==0)
	{
		fclose(fid);
		return mime_type;
	}
     }     
  }
  fclose(fid);
  //par default on dit que c'est de type text/plain
  return "text/plain";


}

int get_file_size(int fd){
	struct stat sb;

	//on recupere les info du fichier
	if(fstat(fd,&sb)==-1){
		return -1;
	}
	return sb.st_size;

}


/* permet d'essaiyer de lire la premiere ligne d'un fichier
  si il n'est pas ouvrable on tue le processus */
char *fgets_or_exit(char *buffer,int size,FILE *stream){
	if(fgets(buffer,size,stream)==NULL){
    exit(0);
	}else{
		return buffer;
	}
}


void skip_headers(FILE *client){
  char buff[TAILLE_BUFF];
  int t=1;
  /* avance dans le fichier tant que 
   on n'a pas de double retour a la ligne */
  while(t){ 
    fgets_or_exit(buff,TAILLE_BUFF,client);
    if(match(buff,"^\r*\n*$")){
      t=0;
    }
  }	
}


char * rewrite_url( char * url ){

	//on verifie qui l'url est un ?
	char * i = strchr(url,'?');
	if(i==NULL){
	}else{
		*i='\0';
	}
	
	//si on demande '/' on renvoie l'index
	if(strcmp(url,"/")==0){
		strcpy(url,"/index.html");
	}
	return url;
}


/* permet de copier le contenu d'un descripteur
   de fichier dans un fichier (ici le client) */
int copy(int in,FILE * out){
	char * buffer[TAILLE_BUFF];
	int t;
	
	while(1){
		t=read(in,buffer,TAILLE_BUFF);
		if(t==-1 || t==0){ return 0;}

		t=fwrite(buffer,1,t,out);
		if(t==-1 || t==0){ return 0;}
	
		
	}
}
