#include "socket.h"
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <signal.h> 
#include <sys/wait.h>

int creer_serveur(int port){
  int socket_serveur ;
  int optval = 1;
  socket_serveur=socket(AF_INET,SOCK_STREAM,0);
  if(socket_serveur==-1){
    perror("socket_serveur");
    return -1;
  }
  struct sockaddr_in saddr;
  saddr.sin_family=AF_INET;
  saddr.sin_port=htons(port);
  saddr.sin_addr.s_addr=INADDR_ANY;

  if(setsockopt(socket_serveur,SOL_SOCKET,SO_REUSEADDR,&optval,sizeof(int))==-1)
    perror(" Can not set SO_REUSEADDR option ");

  if(bind(socket_serveur,(struct sockaddr*)&saddr,sizeof(saddr))==-1){
    perror("bind socket_serveur");
    return -1;
  }
  if(listen(socket_serveur,10)==-1){
    perror("listen socket_serveur");
    return -1;
  }
  return socket_serveur;
}

/* quand un signal est recu on attend le fil 
  pour ne pas qu'il devienne zombie */
void traitement_signal(int sig){
  printf("Signal %d recu\n",sig);
  wait(NULL);
}

/*permet de parametre l'ecoute des signaux
  pour pouvoir detecte la mort d'un enfant */
void initialiser_signaux(){
  struct sigaction sa ;
  sa.sa_handler = traitement_signal ;
  sigemptyset (&sa.sa_mask);
  sa.sa_flags = SA_RESTART ;
  if(sigaction(SIGCHLD,&sa,NULL) == -1){
      perror("sigaction(SIGCHLD)");
  }
  if(signal(SIGPIPE,SIG_IGN) == SIG_ERR){
      perror("signal");
  }
}
