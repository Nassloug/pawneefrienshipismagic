#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <signal.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/udp.h>
#include <netdb.h>
int main(int argc,char ** argv){
  if(argc==3){
    int sock;
    char recvBuff[1024];
    struct hostent *hostinfo;
    struct sockaddr_in sin; /* initialise la structure avec des 0 */
    hostinfo = gethostbyname(argv[1]); /* on récupère les informations de l'hôte auquel on veut se connecter */
    if (hostinfo == NULL) /* l'hôte n'existe pas */
      {
	fprintf (stderr, "Hôte inconnu %s.\n", argv[1]);
	exit(EXIT_FAILURE);
      }
    printf("%s:%i\n", argv[1],hostinfo->h_addr_list[0]);
    memcpy(&sin.sin_addr, hostinfo->h_addr_list[0], hostinfo->h_length);/* l'adresse se trouve dans le champ h_addr de la structure hostinfo */
    sin.sin_port = htons(atoi(argv[2])); /* on utilise htons pour le port */
    sin.sin_family = AF_INET;
			
    if((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0){
      printf("\n Error : Could not create socket \n");
      return 1;
    } 
    if(connect(sock,(struct sockaddr*) &sin, sizeof(struct sockaddr)) == -1)
      {
	perror("connect()");
	exit(EXIT_FAILURE);
      }
    int buffIdx;
    while ((buffIdx=read(sock, recvBuff, sizeof(recvBuff)-1)) > 0)
      {
	recvBuff[buffIdx] = 0;
	if(fputs(recvBuff, stdout) == EOF)
	  {
	    printf("\n Error : Fputs error\n");
	  }
	//write(sock, "lel", sizeof("lel")-1);
	//ne pas decommenter
      } 
    close(sock);
  }
  return 0;
}
